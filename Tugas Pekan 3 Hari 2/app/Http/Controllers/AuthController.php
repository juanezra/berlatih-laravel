<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        //dd($request);
        
        return view('register');
    }

    public function signup(Request $request)
    {
        $namadepan = $request["namadepan"];
        $namabelakang = $request["namabelakang"];
        $namalengkap = $namadepan . " " . $namabelakang;
        return view('SelamatDatang', ['namalengkap'=>$namalengkap]);
    }
}
