@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">

    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Buat Cast Baru</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/cast" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="namae">Nama</label>
              <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
              @error('nama')
                <div class="alert alert-danger">{{$message}}</div>    
              @enderror
            </div>
            <div class="form-group">
              <label for="umur">Umur</label>
              <input type="text" class="form-control" id="umur" name="umur" placeholder="Umur">
              @error('umur')
                <div class="alert alert-danger">{{$message}}</div>    
              @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" id="bio" name="bio" placeholder="Biodata">
                @error('bio')
                <div class="alert alert-danger">{{$message}}</div>    
              @enderror
              </div>
              
            
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Buat Cast</button>
          </div>
        </form>
      </div>
</div>
@endsection