@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">

    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Edit Cast {{$cast->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
          <div class="card-body">
            <div class="form-group">
              <label for="namae">Nama</label>
              <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $cast->nama)}}" placeholder="Nama">
              @error('nama')
                <div class="alert alert-danger">{{$message}}</div>    
              @enderror
            </div>
            <div class="form-group">
              <label for="umur">Umur</label>
              <input type="text" class="form-control" id="umur" name="umur" value="{{ old('umur', $cast->umur)}}" placeholder="Umur">
              @error('umur')
                <div class="alert alert-danger">{{$message}}</div>    
              @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', $cast->bio)}}" placeholder="Biodata">
                @error('bio')
                <div class="alert alert-danger">{{$message}}</div>    
              @enderror
              </div>
              
            
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit Cast</button>
          </div>
        </form>
      </div>
</div>
@endsection