@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Cast</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                <a class= "btn btn-primary mb-2" href="/cast/create"> Buat Cast Baru </a>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th style="width: 40px">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($cast as $key => $cast)
                        <tr>
                            <td> {{ $key + 1 }}</td>
                            <td> {{ $cast->nama }}</td>
                            <td> {{ $cast->umur }}</td>
                            <td> {{ $cast->bio }}</td>
                            <td style="display: flex;">
                                <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">Show</a>
                                <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                                <form action="/cast/{{$cast->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger">
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="5" align="center"> Tidak Ada Data Cast </td>
                            </tr>                
                        
                    @endforelse
                  
                 
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <!--<div class="card-footer clearfix">
              <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">«</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">»</a></li>
              </ul>
            </div>-->
          </div>
    </div>
@endsection