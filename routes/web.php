<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});



Route::get('/Welcome', function () {
    return view('welcome');
});



Route::get('/form', 'AuthController@form');

Route::post('/signup', 'AuthController@signup');

Route::get('/home', 'HomeController@home');
  

